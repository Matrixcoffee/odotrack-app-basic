main: tests/testdata.org
	xbasic --quitonend odotrack.bas; echo

toy:
	xbasic --quitonend tests/test_widget.bas; echo

test_org: tests/testdata.org
	xbasic --quitonend tests/test_org.bas; echo

test_odorg: tests/testdata.org
	xbasic --quitonend tests/test_odorg.bas; echo

test_struct:
	xbasic --quitonend tests/test_struct.bas; echo

tests/testdata.org: tests/generate_test_data.bas tests/csv.bas tests/worldcities.csv
	xbasic --quitonend $< > $@

%.flob: %.bas
	awk 'END{for(i=0;i<nmerges;i++){while(getline l < MERGES[i]){print l}}}/^merge /{printf "%c", 39;m=$$0;sub("^merge[ \t]+\"","",m);sub("\".*$$","",m);MERGES[nmerges++]=m}1' < $< > $@

clean:
	rm -v *.flob */*.flob;:

distclean: clean
	rm -v tests/testdata.org;:
