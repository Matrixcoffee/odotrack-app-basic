# Odotrack
Keep track of your car's odometer and where it was parked.

## What is this?

Odotrack lets you keep a log of locations and odometer (car milage/kilometrage) readouts. The log is stored in plain text form as an [org mode](http://orgmode.org) formatted file.

## Why would I want this?

Usually this kind of log is required for tax purposes.

## Status

This software is pre-alpha, and not useful yet, not even to this author.

## How do I use this?

This software is not ready for use.

That said, for the stubborn:

1. Download or check it out to any location you like.
2. Install [X11Basic](http://x11-basic.sourceforge.net) to your computer and/or mobile phone.
3. There is currently a file named test_widget.bas that you can load up and run from inside the basic environment to demonstrate some working code. If you are on Linux, running `make` will do this automatically.

## Author

[Coffee](https://gitlab.com/Matrixcoffee)

## Contact
* Website: [http://open.source.coffee](http://open.source.coffee)
* Mastodon: [@Coffee@toot.cafe](https://toot.cafe/@Coffee)

## License

Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

The full text of this license can be found in the file called
[LICENSE](https://gitlab.com/Matrixcoffee/odotrack-app-basic/blob/master/LICENSE).

### Basic World Cities Database

This repository includes an unmodified copy of the Basic World Cities Database, as offered by Pareto Software, LLC, the owner of simplemaps.com, under a [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html) [license](tests//worldcities_license.txt). This database is only used for generating test data, and is not used by the actual application.
