' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

print "This is a library file."
print "Executing this file directly is not going to work."
print "See 'test_odorg.bas' for an example on how this file can be used."
end

' #FIXME#: Describe function

function odorg_get_all(node$, var odometer$, var description$, var tags$, var category$, var location$, var car$, var refuel$)
  local lvl%, title$, n%, i%, k$

  lvl% = @org_parse_headline(node$, title$, tags$)
  if lvl% <> 1
    return 0
  endif

  description$ = title$
  odometer$ = @chop$(description$)
  category$ = ""
  ret% = 2

  location$ = @struct_new$("location")
  car$ = @struct_new$("car")
  refuel$ = @struct_new$("refuel")

  n% = @org_get_props(node$, props$())

  i% = 0
  while i% < n%
    k$ = lower$(props$(i%, 0))
    if k$ = "category"
      category$ = props$(i%, 1)
    else if left$(k$, 9) = "location."
      location$ = @struct_set$(location$, @lright$(k$, 10), props$(i%, 1))
    else if left$(k$, 4) = "car."
      car$ = @struct_set$(car$, @lright$(k$, 5), props$(i%, 1))
    else if left$(k$, 7) = "refuel."
      refuel$ = @struct_set$(refuel$, @lright$(k$, 8), props$(i%, 1))
    else
      dec ret%
    endif
    inc ret%
    inc i%
  wend

  return ret%
endfunction

' #FIXME#: Describe function

function odorg_get_location$(node$)
  local i%, n%, lvl%, k$, title$, tags$, props$()
  local location$

  lvl% = @org_parse_headline(node$, title$, tags$)
  if lvl% <> 1
    return ""
  endif

  void @chop$(title$)

  location$ = @struct_new$("location")
  n% = @org_get_props(node$, props$())

  i% = 0
  while i% < n%
    k$ = lower$(props$(i%, 0))
    if left$(k$, 9) = "location."
      location$ = @struct_set$(location$, @lright$(k$, 10), props$(i%, 1))
    endif
    inc i%
  wend

  return location$
endfunction

' Return the rightmost part of the string, starting at the [offset%]th
' character from the left. It is the complement of left$(), as in
'    x$ = left$(x$, x) + @lright$(x$, x + 1).
' It is also exactly like mid$(x$, x, infinity)

function lright$(t$, offset%)
  return right$(t$, len(t$) - offset% + 1)
endfunction
