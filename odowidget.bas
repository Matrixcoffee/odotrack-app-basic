' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

print "This is a library file."
print "Executing this file directly is not going to work."
print "See 'test_widget.bas' for an example on how this file can be used."
end

function edit_odo(oldval, prompt$, ndecimals%)
  local t, kk$, k$, oldval_s$, newval_s$, x$

  oldval_s$ = @format_decimal$(oldval, ndecimals%)
  newval_s$ = oldval_s$
  t = timer
  do
    @show_prompt(prompt$, oldval_s$, newval_s$, x$)
    kk$ = inkey$
    if len(kk$)
      while len(kk$)
        k$ = left$(kk$, 1)
        exit if k$ = chr$(10) or k$ = chr$(13) or k$ = chr$(27)
        kk$ = right$(kk$, len(kk$) - 1)

        if (k$ >= "0" and k$ <= "9" and \
             (@numberofdecimals(x$) < ndecimals% or instr(x$, ".") = 0)) \
           or (k$ = "." and ndecimals% > 0 and instr(x$, ".") = 0)
          x$ = x$ + k$
        endif
        if k$ = chr$(127) or k$ = chr$(7)
          x$ = left$(x$, len(x$) - 1)
        endif
      wend
      if x$ = "" or x$ = "."
        newval_s$ = oldval_s$
      else
        newval_s$ = @update_with_partial$(oldval_s$, x$)
      endif

      exit if k$ = chr$(10) or k$ = chr$(13) or k$ = chr$(27)
      t = timer
    else if timer - t < 10
      pause 0.1
    else
      pause 2
    endif
  loop
  print
  if k$ <> chr$(10) and k$ <> chr$(13)
    return oldval
  endif
  return val(newval_s$)
endfunction

procedure show_prompt(prompt$, oldval$, newval$, x$)
  local i%, f%
  local oldval_int$, oldval_dec$, newval_int$, newval_dec$, x_int$, x_dec$
  f% = -1

  @split_decimal(oldval$, oldval_int$, oldval_dec$)
  @split_decimal(newval$, newval_int$, newval_dec$)
  @split_decimal(x$, x_int$, x_dec$)

  @padleft(oldval_int$, newval_int$, " ")
  @padright(newval_dec$, oldval_dec$, "0")

  print chr$(13); prompt$;
  for i% = 1 to len(newval_int$) - len(x_int$)
    if f%
      if mid$(oldval_int$, i%, 1) <> mid$(newval_int$, i%, 1)
        print COLOR(1, 34);
        f% = 0
      endif
    endif
    print mid$(newval_int$, i%, 1);
  next i%
  print COLOR(1, 31); x$; COLOR(0);
  if x_int$ = ""
    if x_dec$ = ""
      print oldval_dec$;
    else if x_dec$ = "."
      print right$(oldval_dec$, len(oldval_dec$) - 1);
    endif
  endif
  print @clr_eol$();
  flush
endprocedure

' Return the value of [oldval$], updated with partial digits from
' [partial$]. This is the crux of the whole library, yet is too
' complex to fully explain here. A detailed explanation can be found
' in the documentation. #TODO#
function update_with_partial$(oldval$, partial$)
  local newval, ret$, a, d%
  local partial_int$, partial_dec$

  @split_decimal(partial$, partial_int$, partial_dec$)
  a = val(oldval$) / 10^len(partial_int$)

  newval = trunc(a) * 10^len(partial_int$) + val(partial$)
  if newval < val(oldval$)
    newval = trunc(a + 1) * 10^len(partial_int$) + val(partial$)
  endif

  return @format_decimal$(newval, @numberofdecimals(oldval$))
endfunction

' Receive the part before any dot (".") in [intpart$], and receive
' the dot itself and the part after in [decpart$], if present.
' If no dot is present in [orig$], [orig$] is copied to [intpart$] unchanged
' and [decpart$] will be set to the empty string ("").
procedure split_decimal(orig$, var intpart$, var decpart$)
  local s%
  s% = instr(orig$, ".")
  if s%
    intpart$ = left$(orig$, s% - 1)
    decpart$ = mid$(orig$, s%, len(orig$) - s% + 1)
  else
    intpart$ = orig$
    decpart$ = ""
  endif
endprocedure

' Return [dvalue] as a string, rounded and padded to [ndecimals%] decimals.
function format_decimal$(dvalue, ndecimals%)
  local ret$
  dvalue = round(dvalue * 10^ndecimals%)
  ret$ = str$(trunc(dvalue / 10^ndecimals%))
  if ndecimals% > 0
    ret$ = ret$ + "." + right$(str$(round(dvalue)), ndecimals%)
  endif
  return ret$
endfunction

' Pad [dst$] to the length of [src$], adding [padchar$] to the left
' as necessary.
procedure padleft(var dst$, src$, padchar$)
  while len(dst$) < len(src$)
    dst$ = padchar$ + dst$
  wend
endprocedure

' Pad [dst$] to the length of [src$], adding [padchar$] to the right
' as necessary.
procedure padright(var dst$, src$, padchar$)
  while len(dst$) < len(src$)
    dst$ = dst$ + padchar$
  wend
endprocedure

' Count the number of decimals in [v$], which should be
' a number in string form.
deffn numberofdecimals(v$) = len(rightof$(v$, "."))

deffn dbgs$(s$) = chr$(34) + s$ + chr$(34) + " (" + str$(len(s$)) + ") "

deffn clr_eol$() = chr$(27) + "[K"
