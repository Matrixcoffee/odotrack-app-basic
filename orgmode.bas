' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

print "This is a library file."
print "Executing this file directly is not going to work."
print "See 'test_org.bas' for an example on how this file can be used."
end

' Open org-mode formatted file for reading and return an (opaque)
' descriptor. Note that functions and procedures may modify the
' descriptor during use!

function org_open$(filename$)
  local fd%

  fd% = freefile()
  open "I", #fd%, filename$
  return @org_make_descriptor$(fd%)
endfunction

' Convert an (already open) BASIC file descriptor into a descriptor
' object as used by this module. Note that functions and procedures may
' modify the descriptor during use!

function org_make_descriptor$(fileno%)
  return mki$(fileno%)
endfunction

' Internal procedure: Write components into descriptor object

procedure _org_write_descriptor(var org_fd$, fd%, line$)
  org_fd$ = mki$(fd%) + line$
endprocedure

' Internal procedure: Break descriptor object into its constituent parts

procedure _org_extract_descriptor(org_fd$, var fd%, var line$)
  fd% = cvi(left$(org_fd$, 2))
  line$ = mid$(org_fd$, 3, len(org_fd$) - 2)
endprocedure

' Read an org-mode headline node from the org descriptor [org_fd$] and
' return an org node object, or "" on end of file or error.

function org_read_node$(var org_fd$)
  local nodelines%, nodelines$()
  local fd%, line$

  dim nodelines$(30)
  @_org_extract_descriptor(org_fd$, fd%, line$)
  nodelines% = @_org_read_node(fd%, line$, nodelines$())
  @_org_write_descriptor(org_fd$, fd%, line$)
  if nodelines% <= 0
    return ""
  endif

  return @_org_pack_node$(nodelines%, nodelines$())
endfunction

function _org_read_node(fd%, var line$, var nodelines$())
  local nodelines%, lvl%

  nodelines% = 0

  ' Find the start of a node
  do
    lvl% = @_org_headline_level(line$)
    if lvl% > 0
      break
    endif
    if eof(#fd%)
      return 0
    endif
    line$ = lineinput$(#fd%)
  loop

  ' Read into nodelines$() until the start of the next node
  repeat
    @appendarray_str(line$, nodelines%, nodelines$())
    line$ = lineinput$(#fd%)
    lvl% = @_org_headline_level(line$)
  until lvl% > 0 or eof(#fd%)

  return nodelines%
endfunction

' Internal function: wrap up a node array into an opaque object

function _org_pack_node$(nlines%, var node_array$())
  dim node_array$(nlines%)
  return mka$(node_array$())
endfunction

' Parse PROPERTIES drawer of [node$] into [props$()]. The result is a
' two-dimensional array, with the property name in [props$(x, 0)] and
' the property's value in [props$(x, 1)].
'
' Returns the number of properties read, 0 if no properties were
' present, or -1 on error.

function org_get_props(node$, var props$())
  local nodelines$(), nprops%, i%
  local propname$, propvalue$

  nodelines$() = cva(node$)

  i% = 0
  nprops% = -1
  while i% < dim?(nodelines$())
    if trim$(nodelines$(i%)) = ":END:"
      return nprops%
    endif
    if nprops% >= 0
      @_org_parse_propline(nodelines$(i%), propname$, propvalue$)
      dim props$(nprops% + 1, 2)
      props$(nprops%, 0) = propname$
      props$(nprops%, 1) = propvalue$
      inc nprops%
    endif
    if trim$(nodelines$(i%)) = ":PROPERTIES:"
      nprops% = 0
    endif
    inc i%
  wend
  return 0
endfunction

' Parse a properties line into its components, [$propname] and [$propvalue].

procedure _org_parse_propline(line$, var propname$, var propvalue$)
  propname$ = leftof$(rightof$(line$, ":"), ":")
  propvalue$ = trim$(rightof$(rightof$(line$, ":"), ":"))
endprocedure

' Get time stamps from [$node].
'
' Returns 0 if not present, otherwise the number of timestamps read into
' [stamps$()] is returned. The result is a two-dimensional array, with
' the time stamp name in [stamps$(x, 0)] and the time stamp's value in
' [stamps$(x, 1)].

function org_get_ts(node$, var stamps$())
  local nodelines$()

  nodelines$() = cva(node$)
  if dim?(nodelines$()) < 2
    return 0
  endif
  return @_org_parse_ts(nodelines$(1), stamps$())
endfunction

' See if the line [line$] contains timestamps of some form.
' Returns 0 if no match, otherwise the number of timestamps read
' into [stamps$()] is returned.

function _org_parse_ts(line$, var stamps$())
  local i%, w$, qualifier$, ts$, eb$, origline$
  origline$ = line$

  if line$ = "" \
      or left$(line$, 1) = " " \
      or tally(line$, "<") <> tally(line$, ">") \
      or tally(line$, "[") <> tally(line$, "]")
    return 0
  endif

  clr stamps$()
  i% = 0
  qualifier$ = ""
  ts$ = ""

  while line$ <> ""
    eb$ = ""
    w$ = @chop$(line$)
    if left$(w$, 1) = "<"
      eb$ = ">"
    else if left$(w$, 1) = "["
      eb$ = "]"
    else if right$(w$, 1) = ":"
      if qualifier$ <> ""
        print "Double qualifier encountered. Abort."
        return 0
      endif
      qualifier$ = w$
    else
      print "Unknown element encountered. Abort."
      return 0
    endif

    if eb$ <> ""
      while eb$ <> "" and right$(w$, 1) <> eb$
	w$ = w$ + " " + @chop$(line$)
      wend
      if right$(w$, 1) <> eb$
        print "Unmatched bracket encountered. Abort."
        return 0
      endif
      dim stamps$(i% + 1, 2)
      stamps$(i%, 0) = qualifier$
      stamps$(i%, 1) = w$
      qualifier$ = ""
      w$ = ""
      inc i%
    endif
  wend
  return i%
endfunction

' Parse the components of the headline of a headline node.
' Returns 0 on errors, or the headline level on success. [title$] will
' then contain the title and [tags$] will contain the raw tag string.

function org_parse_headline(node$, var title$, var tags$)
  local nodelines$(), lvl$, headline$

  if node$ = ""
    return 0
  endif

  nodelines$() = cva(node$)

  if dim?(nodelines$()) <= 0
    return 0
  endif

  headline$ = nodelines$(0)
  lvl% = @_org_headline_level(headline$)
  split rightof$(headline$, " "), " :", 0, title$, tags$
  if tags$ <> ""
    tags$ = ":" + tags$
  endif
  return lvl%
endfunction

' See if the line [line$] is an org-mode heading. Returns 0 if no match,
' otherwise the heading level is returned.

function _org_headline_level(line$)
  local stars$
  stars$ = @chop$(line$)
  if stars$ = "" or len(stars$) <> tally(stars$, "*")
    return 0
  endif
  return len(stars$)
endfunction

' Utility function: return the first word of [text$] and modify [text$]
' to contain the remainder.

function chop$(var text$)
  local ret$
  split text$, " ", 0, ret$, text$
  return ret$
endfunction

' Append [value$] to the array of strings [the_array$()]. [nvalues%]
' should be the number of useful elements in the array, and will be
' modified in place. The array will be grown if necessary.

procedure appendarray_str(value$, var nvalues%, var the_array$())
  if nvalues% >= dim?(the_array$())
    dim the_array$(1 + nvalues% * 2)
  endif

  the_array$(nvalues%) = value$
  inc nvalues%
endprocedure

'Produce a time stamp suitable for inclusion in an org-mode file.
'
' [d$] is the date and [t$] is the time in standard BASIC format, as
' returned by date$ and time$, respecitively.

function org_make_ts$(d$, t$)
  local dow$()

  dow$() = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

  return right$(d$, 4) + "-" + mid$(d$, 4, 2) + "-" + left$(d$, 2) + " " \
    + dow$(julian(d$) mod 7) + " " + left$(time$, 5)
endfunction
