' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

print "This is a library file."
print "Executing this file directly is not going to work."
print "See 'test_org.bas' for an example on how this file can be used."
end

' Return a new empty struct

function struct_new$(name$)
  local astruct$()
  @astruct_new(astruct$(), name$)
  return mka$(astruct$())
endfunction

procedure astruct_new(var astruct$(), name$)
  dim astruct$(1, 3)
  clr astruct$()
  astruct$(0, 0) = name$
endprocedure

' Return a copy of [struct$] with its member [name$] set to [value$]

function struct_set$(struct$, name$, value$)
  local astruct$()
  astruct$() = cva(struct$)
  @astruct_set(astruct$(), name$, value$)
  return mka$(astruct$())
endfunction

' Modify [astruct$()] in-place to change or add member [$name] and set
' it to [value$].
'
' This procedure operates on array structs instead of string structs. I
' have not yet decided if this is for internal use only.

procedure astruct_set(var astruct$(), name$, value$)
  local i%, u%

  i% = @_astruct_lookup(astruct$(), name$)
  u% = ubound(astruct$())

  if i% >= u%
    dim astruct$(i% + 1, 3)
  endif

  astruct$(i%, 0) = name$
  astruct$(i%, 1) = value$
endprocedure

' Return the value of [struct$]'s member [name$].
'
' When the return value is "" this either means the member was not
' found, or its value was set to actual "". To find out the difference,
' use struct_exists().

function struct_get$(struct$, name$)
  local astruct$()
  astruct$() = cva(struct$)
  return @astruct_get$(astruct$(), name$)
endfunction

' Return the value of array struct [astruct$()]'s member [name$].
'
' When the return value is "" this either means the member was not
' found, or its value was set to actual "". To find out the difference,
' use astruct_exists().

function astruct_get$(var astruct$(), name$)
  local i%

  i% = @_astruct_lookup(astruct$(), name$)
  if i% >= ubound(astruct$())
    return ""
  endif

  return astruct$(i%, 1)
endfunction

' Return TRUE if [name$] is a member of [struct$], FALSE if not.

function struct_exists(struct$, name$)
  local astruct$()
  astruct$() = cva(struct$)
  return @astruct_exists(astruct$(), name$)
endfunction

' Return TRUE if [name$] is a member of [astruct$()], FALSE if not.

function astruct_exists(var astruct$(), name$)
  local i%
  i% = @_astruct_lookup(astruct$(), name$)
  return i% < ubound(astruct$())
endfunction

' Internal function. Look up the array position of [name$]. If [name$]
' is not found, the returned value will be one higher than the highest
' possible index.

function _astruct_lookup(var astruct$(), name$)
  local i%, u%

  i% = 1
  u% = ubound(astruct$())

  while i% < u%
    exit if astruct$(i%, 0) = name$
    inc i%
  wend

  return i%
endfunction

function struct_len(struct$)
  local astruct$()
  astruct$() = cva(struct$)
  return @astruct_len(astruct$())
endfunction

function astruct_len(var astruct$())
  return ubound(astruct$()) - 1
endfunction

procedure struct_print(struct$)
  local astruct$()
  astruct$() = cva(struct$)
  @astruct_print(astruct$())
endprocedure

procedure astruct_print(var astruct$())
  local i%, u%

  i% = 1
  u% = ubound(astruct$())

  print "Structure "; astruct$(0, 0); " ("; u% - 1; "):"
  while i% < u%
    print astruct$(0, 0); "."; astruct$(i%, 0); "="; enclose$(astruct$(i%, 1))
    inc i%
  wend
endprocedure
