' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

print "This is a library file."
print "Executing this file directly is not going to work."
end

procedure csv_read(var csv$(), filename$)
  local fh%, r%, c%, line$, rest$, field$

  clr csv$()

  fh% = freefile()
  open "I", #fh%, filename$

  r% = 0
  repeat
    lineinput #fh%, line$
    inc r%
  until line$ = ""

  close #fh%

  if r% >= ubound(csv$())
    dim csv$(r% + 1, ubound(csv$(), 1))
    'print "dim: ("; ubound(csv$()); ", "; ubound(csv$(), 1); ")"
  endif

  fh% = freefile()
  open "I", #fh%, filename$

  ' #FIXME#: Avoid reading 1 blank line that isn't actually there (I did a dumb)

  r% = 0
  repeat
    c% = 0

    if r% >= ubound(csv$())
      dim csv$(r% + 1, ubound(csv$(), 1))
      'print "dim: ("; ubound(csv$()); ", "; ubound(csv$(), 1); ")"
    endif

    lineinput #fh%, line$
    rest$ = line$

    while rest$ <> ""
      if c% >= ubound(csv$(), 1)
        dim csv$(ubound(csv$()), c% + 1)
        'print "dim: ("; ubound(csv$()); ", "; ubound(csv$(), 1); ")"
      endif
      'print "w: ("; r%; ", "; c%; ") -> ("; ubound(csv$()); ", "; ubound(csv$(), 1); ")"
      split rest$, ",", 0, field$, rest$
      csv$(r%, c%) = declose$(field$)
      inc c%
    wend
    inc r%
  until line$ = ""

  close #fh%
endprocedure
