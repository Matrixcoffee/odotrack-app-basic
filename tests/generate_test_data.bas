' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

merge "tests/csv.bas"

odo = 3133.7
stderr% = -1

dim cities$(1, 1)
@csv_read(cities$(), "tests/worldcities.csv")

dim cities_coords(1)
@make_coord_array(cities_coords(), cities$())

fuel = 33

location% = @find_city(cities$(), "Amsterdam")

props$ = @properties_from_city$(cities$(), location%)
props$ = @add_property$(props$, "LOCATION.STREET", "Stationsplein")
props$ = @add_property$(props$, "LOCATION.POSTCODE", "1012 AB")
props$ = @add_property$(props$, "LOCATION.BUILDING", "15")
props$ = @add_property$(props$, "LOCATION.SHORTCODE", "AMS")
props$ = @add_property$(props$, "CAR.NAME", "Generic Car")
props$ = @add_property$(props$, "CAR.LICENSEPLATE", "AA-AA-AA")
props$ = @add_property$(props$, "CAR.ODOMETER", str$(round(odo, 1)))
props$ = @add_property$(props$, "CATEGORY", "work")
node$ = @make_node$(odo, cities$(location%, 0), @make_stamp$(hop% * 2 - 1, hop% * 2), props$, "")
node$ = @make_node$(odo, cities$(location%, 0), @make_stamp$(0, -1), props$, "Car comes into possession")
print #stderr%, node$;
output$ = node$

dim visited%(0)

for hop% = 1 to 20
  prev_location% = location%
  dim visited%(ubound(visited%()) + 1)
  visited%(ubound(visited%()) - 1) = location%
  location% = @find_next_city(cities_coords(), visited%())
  distance = @haversine_distance_km_c(cities_coords(), prev_location%, location%)
  sub fuel, distance / 15
  add odo, distance
  props$ = @properties_from_city$(cities$(), location%)
  if cities$(location%, 1) = "The Hague"
    props$ = @add_property$(props$, "LOCATION.STREET", "Theo Mann-Bouwmeesterlaan")
    props$ = @add_property$(props$, "LOCATION.POSTCODE", "2597 GV")
    props$ = @add_property$(props$, "LOCATION.BUILDING", "75")
  else if cities$(location%, 1) = "Paris"
    props$ = @add_property$(props$, "LOCATION.STREET", "avenue Anatole France")
    props$ = @add_property$(props$, "LOCATION.POSTCODE", "75007")
    props$ = @add_property$(props$, "LOCATION.BUILDING", "5")
    props$ = @add_property$(props$, "LOCATION.SHORTCODE", "EIF")
  endif
  if fuel < 20
    props$ = @add_property$(props$, "REFUEL.VOLUME", str$(round(40 - fuel, 2)))
    props$ = @add_property$(props$, "REFUEL.COST", str$(round((40 - fuel) * (129 + hop% * 2) / 100, 2)))
    fuel = 40
    props$ = @add_property$(props$, "REFUEL.FILL", str$(round(100 * fuel / 40, 2)) + "%")
  endif
  props$ = @add_property$(props$, "CAR.NAME", "Generic Car")
  props$ = @add_property$(props$, "CAR.LICENSEPLATE", "AA-AA-AA")
  props$ = @add_property$(props$, "CAR.ODOMETER", str$(round(odo, 1)))
  props$ = @add_property$(props$, "CATEGORY", "work")
  node$ = @make_node$(odo, cities$(location%, 0), @make_stamp$(hop% * 2 - 1, hop% * 2), props$, "")
  print #stderr%, node$;
  output$ = node$ + output$
next hop%

' Print out the generated nodes in reverse order.
print output$;

end

' Generate a fake arrive/depart line, using make_date$. Set to -1 to
' disable. e.g. to generate a planning line with only the 2nd arrival
' time and no departure time, call @make_stamp$(2, -1).

function make_stamp$(arrive%, depart%)
  local ret$
  ret$ = ""
  if depart% >= 0
    ret$ = "DEADLINE: <" + @make_date$(depart%) + ">"
  endif
  if arrive% >= 0
    if ret$ <> ""
      ret$ = ret$ + " "
    endif
    ret$ = ret$ + "SCHEDULED: <" + @make_date$(arrive%) + ">"
  endif
  return ret$
endfunction

' Generate a fake date/time stamp based on [seq%], which must be zero or
' greater. Dates generated from a higher sequence number are guaranteed
' to be later than those generated from lower sequence numbers.

function make_date$(seq%)
  local mdays(), dow$(), y%, m%, d%, hh%, mm%, jd$
  mdays%() = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  dow$() = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

  y% = 2000 + seq%
  m% = (seq% mod 12) + 1
  inc seq%
  d% = (seq% mod mdays%(m% - 1)) + 1
  inc seq%
  hh% = seq% mod 24
  inc seq%
  mm% = seq% mod 60

  jd$ = @i2$(d%) + "." + @i2$(m%) + "." + @i4$(y%)
  return @i4$(y%) + "-" + @i2$(m%) + "-" + @i2$(d%) + " " \
    + dow$(julian(jd$) mod 7) + " " + @i2$(hh%) + ":" + @i2$(mm%)
endfunction

function i2$(i%)
  return using$(i%, "00")
endfunction

function i4$(i%)
  return using$(i%, "0000")
endfunction

' Generate a full org-mode headline node, with odometer value [odo],
' location name [$name], optional planning line [$planning] and optional
' body [body$].

function make_node$(odo, name$, planning$, properties$, body$)
  local r$
  r$ = "* " + str$(round(odo, 1)) + " " + name$ + chr$(10)
  if properties$ <> ""
    @add_line(r$, "  :PROPERTIES:")
    @add_line(r$, properties$)
    @add_line(r$, "  :END:")
  endif
  if planning$ <> ""
    @add_line(r$, planning$)
  endif
  if body$ <> ""
    @add_line(r$, chr$(10) + body$)
  endif

  return r$ + chr$(10)
endfunction

function add_property$(properties$, name$, value$)
  local ret$, rest$, maxlen%, n$, v$

  properties$ = properties$ + ":" + name$ + ": " + value$

  ret$ = ""
  rest$ = properties$
  maxlen% = len("PROPERTIES")
  while rest$ <> ""
    split rest$, ":", 0, n$, rest$
    split rest$, ":", 0, n$, rest$
    split rest$, chr$(10), 0, v$, rest$
    n$ = trim$(n$)
    maxlen% = max(maxlen%, len(n$))
  wend

  ret$ = ""

  while properties$ <> ""
    split properties$, ":", 0, n$, properties$
    split properties$, ":", 0, n$, properties$
    split properties$, chr$(10), 0, v$, properties$
    n$ = trim$(n$)
    v$ = trim$(v$)
    ret$ = ret$ + "  :" + n$ + ":"
    if v$ <> ""
      ret$ = ret$ + " " + string$(maxlen% - len(n$), " ") + v$
    endif
    ret$ = ret$ + chr$(10)
  wend

  return ret$
endfunction

' Append a line to string [s$] and ensure it is newline-terminated.

procedure add_line(var s$, line$)
  s$ = s$ + line$
  if right$(line$, 1) <> chr$(10)
    s$ = s$ + chr$(10)
  endif
endprocedure

' sum the digits of [a]. e.g.: 123 = 1 + 2 + 3 = 6.
' Keep doing this if the outcome is more than one digit.
' i.e. 567 = 5 + 6 + 7 = 18 = 1 + 8 = 9.
'
' [fmt$] determines how the number is converted to a string for chopping
' into digits. If unsure, use "%d" for integers and "%f" for floats.

function sum_digits(a, fmt$)
  local a$, ret
  ret = 0
  a$ = using$(a, fmt$)
  for i% = 1 to len(a$)
    if glob(mid$(a$, i%, 1), "[0-9]")
      add ret, val(mid$(a$, i%, 1))
    endif
  next i%
  if ret > 9
    return @sum_digits(ret, fmt$)
  else
    return ret
  endif
endfunction

' Add two distances together. This function is meant to "enrich" the
' output with a pseudorandom tenths digit, when only integers are
' available for input. The output value is solely dependent on the value
' of its inputs.

function add_distance(a, b)
  ret = int(a + b) + @sum_digits(a + b, "%.1f") / 10
  return ret
endfunction

' Look up a city by name.

function find_city(var cities$(), city$)
  local r%

  r% = 1
  while r% < ubound(cities$())
    if cities$(r%, 0) = city$
      return r%
    endif
    inc r%
  wend

  return -1
endfunction

' Look up the nearest unvisited city to the last one visited.

function find_next_city(var cities_coords(), var visited%())
  local current_city%, ret%, best_d, r%, doit%, i%, d

  current_city% = visited%(ubound(visited%()) - 1)
  lat = cities_coords(current_city%, 0)
  lon = cities_coords(current_city%, 1)

  ret% = 1
  doit% = true
  while ret% < ubound(cities_coords())
    i% = 0
    while i% < ubound(visited%())
      if visited%(i%) = r%
        doit% = false
        break
      endif
      inc i%
    wend
    break if not doit%
    inc ret%
  wend

  best_d = @geodistance1(cities_coords(), ret%, lat, lon)

  r% = 1
  while r% < ubound(cities_coords())
    'print r%; chr$(13);
    doit% = true
    i% = 0
    while i% < ubound(visited%())
      if visited%(i%) = r%
        doit% = false
        break
      endif
      inc i%
    wend

    if doit%
      d = @geodistance1(cities_coords(), r%, lat, lon)
      if d < best_d
        best_d = d
        ret% = r%
      endif
    endif

    inc r%
  wend

  return ret%
endfunction

' Populate a properties object from the data in [cities$()] pointed at by [city%].

function properties_from_city$(var cities$(), city%)
  local ret$

  ret$ = ""
  ret$ = @add_property$(ret$, "LOCATION.GEO", "geo:" + cities$(city%, 2) + "," + cities$(city%, 3))
  ret$ = @add_property$(ret$, "LOCATION.CITY", cities$(city%, 1))
  return ret$
endfunction

' Dumb distance calculation: return naive distance between entries [a%]
' and [b%] in the city coordinates table.

function geodistance(var cities_coords(), a%, b%)
  local lat_a, lon_a, lat_b, lon_b

  lat_a = cities_coords(a%, 0)
  lon_a = cities_coords(a%, 1)
  lat_b = cities_coords(b%, 0)
  lon_b = cities_coords(b%, 1)
  return hypot(lat_b - lat_a, lon_b - lon_a)
endfunction

' Dumb distance calculation: return naive distance between entry [city%]
' in the city coordinates table and the position at [lat%], [lon%].

function geodistance1(var cities_coords(), city%, lat, lon)
  local lat2, lon2

  lat2 = cities_coords(city%, 0)
  lon2 = cities_coords(city%, 1)
  return hypot(lat2 - lat, lon2 - lon)
endfunction

' Convert and subset the [cities$] database into the coordinates table
' [coords()].

procedure make_coord_array(var coords(), cities$())
  local i%, u%

  dim coords(ubound(cities$()), 2)
  i% = 0
  u% = ubound(cities$())
  while i% < u%
    coords(i%, 0) = val(cities$(i%, 2))
    coords(i%, 1) = val(cities$(i%, 3))
    inc i%
  wend
endprocedure

' Calculate haversine distance between entries [a%] and [b%] in the city
' coordinates table.

function haversine_distance_km_c(var cities_coords(), a%, b%)
  local lat_a, lon_a, lat_b, lon_b

  lat_a = cities_coords(a%, 0)
  lon_a = cities_coords(a%, 1)
  lat_b = cities_coords(b%, 0)
  lon_b = cities_coords(b%, 1)
  return @haversine_distance_km(lat_a, lon_a, lat_b, lon_b)
endfunction

' Calculate approximate distance in km between two coordinates (in
' degrees) using the haversine function.

function haversine_distance_km(lat1, lon1, lat2, lon2)
  local rlat, rlon, a, km

  rlat = rad(lat2 - lat1)
  rlon = rad(lon2 - lon1)

  a = sin(rlat / 2) ^ 2 + cos(rad(lat1)) * cos(rad(lat2)) * sin(rlon / 2) ^ 2
  km = 6367 * 2 * atan2(sqr(a), sqr(1 - a))

  return km
endfunction
