' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

merge "orgmode.bas"
merge "odorgmode.bas"
merge "struct.bas"

org_file$ = @org_open$("tests/testdata.org")

i% = 3

while i%
  node$ = @org_read_node$(org_file$)
  exit if node$ = ""

  location$ = @odorg_get_location$(node$)
  ret% = @odorg_get_all(node$, odometer$, description$, tags$, category$, location$, car$, refuel$)

  print "Odometer value:   "; enclose$(odometer$)
  print "Stop description: "; enclose$(description$)
  print "Stop category:    "; enclose$(category$)
  print "Tags:             "; enclose$(tags$)

  @struct_print(location$)
  @struct_print(car$)
  @struct_print(refuel$)
  print

  dec i%
wend


end
