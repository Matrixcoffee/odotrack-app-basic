' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

merge "orgmode.bas"

org_file$ = @org_open$("tests/testdata.org")

i% = 3

while i%
  node$ = @org_read_node$(org_file$)
  exit if node$ = ""

  rawnode$() = cva(node$)
  @dump_array$("  ", rawnode$())

  lvl% = @org_parse_headline(node$, title$, tags$)
  print "Level: "; lvl%; " Title: "; enclose$(title$); " Tags: "; enclose$(tags$)

  n% = @org_get_props(node$, props$())
  print "Properties: "; n%
  j% = 0
  while j% < n%
    print COLOR(1, 36);
    print "  Property: "; enclose$(props$(j%, 0))
    print "  Value:    "; enclose$(props$(j%, 1))
    print COLOR(0);
    inc j%
  wend

  n% = @org_get_ts(node$, stamps$())
  j% = 0
  while j% < n%
    print COLOR(1, 35);
      print "  Keyword:   "; enclose$(stamps$(j%, 0))
      print "  Timestamp: "; enclose$(stamps$(j%, 1))
    print COLOR(0);
    inc j%
  wend

  dec i%
wend

print
print "Current time in org format: <" + @org_make_ts$(date$, time$) + ">"


end

procedure dump_array$(prefix$, var the_array$())
  local u$, i%
  if (dim?(the_array$()) <= 0)
    return
  endif
  u$ = string$(len(str$(dim?(the_array$()) - 1)), "#") + " "
  for i% = 0 to dim?(the_array$()) - 1
    print prefix$; i% using u$; the_array$(i%)
  next i%
endprocedure
