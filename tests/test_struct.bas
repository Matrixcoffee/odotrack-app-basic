' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

merge "struct.bas"

s$ = @struct_new$("test")
print "Struct length: "; @struct_len(s$)
print "-----dump-----"
@struct_print(s$)
print "---end dump---"
print "added exists: "; @struct_exists(s$, "added")
print "value of added: "; enclose$(@struct_get$(s$, "added"))
s$ = @struct_set$(s$, "added", "yes")
print "added exists: "; @struct_exists(s$, "added")
print "value of added: "; enclose$(@struct_get$(s$, "added"))
print "Struct length: "; @struct_len(s$)
print "-----dump-----"
@struct_print(s$)
print "---end dump---"

end
