' Copyright (C) 2020 by Coffee (@Coffee@toot.cafe)

' you may redistribute and/or modify this file under the terms of
' the GNU Affero General Public License, version 3 or later. The full
' text of this license can be found in the file called LICENSE, which
' should have been distributed along with this file. If not, try the
' following sources:

' https://gitlab.com/Matrixcoffee/sixel-experiments/blob/master/LICENSE
' https://spdx.org/licenses/AGPL-3.0-or-later.html
' https://www.gnu.org/licenses/

' SPDX-License-Identifier: AGPL-3.0-or-later

cls
merge "odowidget.bas"

oldodo = 592987
newodo = @edit_odo(oldodo, "odometer> ", 0)
print "odometer (old): "; oldodo
print "odometer (new): "; newodo
print "distance: "; newodo - oldodo

oldodo = 592987.2
newodo = @edit_odo(oldodo, "odometer> ", 1)
print "odometer (old): "; oldodo
print "odometer (new): "; newodo
print "distance: "; newodo - oldodo

end
